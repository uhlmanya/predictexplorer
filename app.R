library(shiny)
library(ggplot2)
library(httr)
library(jsonlite)
library(dplyr)
library(data.table)

options(browser = "firefox")

ui <- fluidPage( titlePanel("PREDICT Explorer") 
               , sidebarLayout( sidebarPanel( sliderInput( "slider_W", "W [μm]:"
                                                         , min = 0.5
                                                         , step = 0.1
                                                         , max = 5.0
                                                         , value =  2.5 )
                                            , checkboxInput( "check_L", "All L:"
                                                           , value = TRUE, width = NULL)
                                            , sliderInput( "slider_L", "L [nm]:"
                                                         , min = 150
                                                         , max = 1450
                                                         , step = 100
                                                         , value =  250 )
                                            , radioButtons( "radio_xaxis", "X-Axis:"
                                                          , c("gm/id" = "gmid"
                                                             ,"vdsat" = "vdsat") )
                                            , sliderInput( "slider_Vds", "Vds:"
                                                         , min = 0.01
                                                         , max = 1.20
                                                         , step = 0.01
                                                         , round = -2
                                                         , value =  0.6 )
                                            , sliderInput( "slider_Vgs", "Vgs:"
                                                         , min = 0.01
                                                         , step = 0.01
                                                         , max = 1.20
                                                         , round = -2
                                                         , value =  0.6 ) 
                                            , textInput( "modelServer", "Server Address:"
                                                       , value = "134.103.192.238:8080/predict"
                                                       , placeholder = "IP:PORT/ROUTE" ) 
                                            , "Timed Function Call [s]:"
                                            , verbatimTextOutput( "text_ping"
                                                                , placeholder = TRUE ) )
                              , mainPanel( plotOutput("plot_tfc") 
                                         , verbatimTextOutput( "text_tfc", placeholder = TRUE )
                                         , plotOutput("plot_opc") 
                                         , verbatimTextOutput( "text_opc", placeholder = TRUE )
                                         ) ) )

server <- function(input, output, session) {
    ping <- reactiveVal(system.time(0))
    tfcCall <- reactiveVal("")
    opcCall <- reactiveVal("")
    inputTransfer <- reactive({
        data.frame( Vgs = seq(0.0, 1.2, by = 0.01)
                  , Vds = rep(input$slider_Vds, 121)
                  , Vbs = rep(0.0, 121)
                  , W = rep(input$slider_W * 10^(-6), 121)
                  , L = rep(input$slider_L * 10^(-9), 121) )        
    })
    inputOuput <- reactive({
        data.frame( Vgs = rep(input$slider_Vgs, 121)
                  , Vds = seq(0.0, 1.2, by = 0.01)
                  , Vbs = rep(0.0, 121)
                  , W = rep(input$slider_W * 10^(-6), 121)
                  , L = rep(input$slider_L * 10^(-9), 121) )        
    })
    dataTransfer <- reactive({
        t <- system.time(res <- POST( input$modelServer
                                    , body = as.list(inputTransfer())
                                    , encode = "json" ))
        ping(t)
        tfcCall(sprintf( "curl -X POST -H \"Content-Type: application/json\" -d '%s' %s"
                       , toJSON(as.list(inputTransfer())), input$modelServer))
        as.data.frame(fromJSON(content(res, as = "text")))
    })
    dataOutput <- reactive({
        t <- system.time(res <- POST( input$modelServer
                                    , body = as.list(inputOuput())
                                    , encode = "json" ))
        ping(t)
        opcCall(sprintf( "curl -X POST -H \"Content-Type: application/json\" -d '%s' %s"
                       , toJSON(as.list(inputOuput())), input$modelServer))
        as.data.frame(fromJSON(content(res, as = "text")))
    })
    output$plot_tfc <- renderPlot({
        opp <- data.frame( Vgs = inputTransfer()$Vgs
                         , Id = dataTransfer()$id )
        ggplot(opp, aes(x = Vgs, y = Id)) +
            geom_point() +
            geom_smooth(method = 'loess', se = TRUE) +
            title("Transfer Characterisitc") +
            xlab("Vgs [V]") +
            ylab("Id [A]")
    })
    output$plot_opc <- renderPlot({
        opp <- data.frame( Vds = inputOuput()$Vds
                         , Id = dataOutput()$id )
        ggplot(opp, aes(x = Vds, y = Id)) +
            geom_point() +
            geom_smooth(method = 'loess', se = TRUE) +
            title("Output Characterisitc") +
            xlab("Vds [V]") +
            ylab("Id [A]")
    })
    output$text_ping <- renderText({ ping()["elapsed"] })
    output$text_tfc <- renderText({ tfcCall() })
    output$text_opc <- renderText({ opcCall() })
}
    
app <- shinyApp( ui = ui, server = server,
               , options = list( host = system("curl -q ip.sb", intern = TRUE) 
                               , port = 5238
                               , launch.browser = FALSE ))
if (interactive()) {
    app
} else {
    runApp(app)
}
